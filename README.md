# Branch Details

Display all branches the current commit belongs to.

### List of all branches that contain this commit will appear in top right
![Highlight 1](media/highlight1.png)

### First four branches will appear with option to "View all"
![Highlight 2](media/highlight2.png)

### When "View all" is pressed all branches will be listed
![Highlight 3](media/highlight3.png)